import { Fragment } from "react"
import nurdCoverlarge from "../images/nurd-cover-large.png"

const Test = () =>{
    return(
        <Fragment>
            <img src={nurdCoverlarge} alt="" />
        </Fragment>
    )
}

export default Test;